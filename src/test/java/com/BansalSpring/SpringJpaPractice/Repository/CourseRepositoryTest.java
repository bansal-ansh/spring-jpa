package com.BansalSpring.SpringJpaPractice.Repository;

import com.BansalSpring.SpringJpaPractice.entity.Course;
import com.BansalSpring.SpringJpaPractice.entity.Student;
import com.BansalSpring.SpringJpaPractice.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseRepositoryTest {
    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void printCourse(){
        List<Course> courses=courseRepository.findAll();
        System.out.println("courses=" + courses);
    }

    @Test
    public void saveCourseWithTeacher(){
        Teacher teacher=Teacher.builder()
                .firstName("Akshit")
                .lastName("Malik")
                .build();
        Course course=Course
                .builder()
                .title("Devops")
                .credit(4)
                .teacher(teacher)
                .build();
        courseRepository.save(course);
    }

    @Test
    public  void findALLPagination(){
        Pageable firstPageWithThreeRecords=
                PageRequest.of(0,3);
        Pageable secondPageWithTwoRecords =
                PageRequest.of(1,2);
        List<Course> courses=courseRepository.findAll(firstPageWithThreeRecords).getContent();
        long totalElements=courseRepository.findAll(firstPageWithThreeRecords).getTotalElements();
        long totalPages=courseRepository.findAll(firstPageWithThreeRecords).getTotalPages();

        System.out.println("totalPages =" +totalPages);
        System.out.println("totalElements =" +totalElements);
        System.out.println("Courses =" +courses);

    }
    @Test
    public void saveCourseWithStudentAndTeacher(){
        Teacher teacher=Teacher.builder()
                .firstName("Shantanu")
                .lastName("Goyal")
                .build();
        Student student=Student.builder()
                .firstName("Tanvi")
                .lastName("Gupta")
                .emailId("tanvi@gmail.com")
                .build();
        Course course=Course
                .builder()
                .title("QA")
                .credit(9)
                .teacher(teacher)
                .build();
        course.addStudents(student);

        courseRepository.save(course);
    }


}